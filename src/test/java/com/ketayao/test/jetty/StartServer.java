package com.ketayao.test.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * 使用Jetty运行调试Web应用, 在Console输入r快速重新加载应用.
 * 
 */
public class StartServer {
    public static final int    PORT     = 9090;
    public static final String CONTEXT  = "/";
    public static final String BASE_URL = "http://localhost:" + PORT + CONTEXT;
    private static final String DEFAULT_WEBAPP_PATH = "G:/myproject/mvndemo/mvndemo/src/main/webapp";
   	private static final String WINDOWS_WEBDEFAULT_PATH = "jetty/webdefault-windows.xml";
    public static void main(String[] args) throws Exception {
        // 启动Jetty
    	Server server=new Server(PORT);
    	WebAppContext context=new WebAppContext(DEFAULT_WEBAPP_PATH, CONTEXT);
    	context.setDefaultsDescriptor(WINDOWS_WEBDEFAULT_PATH);
		server.setHandler(context);
        server.setStopAtShutdown(true);
        try {
            server.start();
            
            System.out.println("启动成功，请使用该路径访问系统：" + BASE_URL);
            System.out.println("在控制台输入'r'重新加载应用，输入'q'退出jetty程序！");

            while (true) {
                char c = (char) System.in.read();
                if (c == 'r') {
                    JettyFactory.reloadContext(server);
                } else if (c == 'q') {
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            System.exit(-1);
        }
    }
}
