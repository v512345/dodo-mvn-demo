package aihsf.mvndemo;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;

public class MultipartUtil {
	public static Map<String,String> multiImg(HttpServletRequest request,String path) {
		Map<String,String> map=new HashMap<String,String>();

		try {
			t2(request,path,map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return map;  
	}


	private void t3(HttpServletRequest request,String path,Map<String,String> map){
		try {
			MultipartParser parser=new MultipartParser(request,5*1024*1024); 
			Part _part=null; 
			while((_part=parser.readNextPart())!=null){ 
				if(_part.isFile()) {  
					FilePart fpart=(FilePart)_part;  
					String fileName=fpart.getFileName();  
					if(fileName!=null)  {  
						fpart.writeTo(new File(path));  
						map.put("name", fileName);
					}  
				}  
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void t(HttpServletRequest request,String path,Map<String,String> map) throws IOException{
		int maxPostSize = 1 * 100 * 1024 * 1024;   
		MultipartRequest mr = new MultipartRequest(request, path, maxPostSize, "UTF-8");   
		Enumeration files = mr.getFileNames();   
		String fileName = "";   
		String filePath=""; 

		while (files.hasMoreElements()) {   
			fileName = (String) files.nextElement();   
			System.out.println("FileName============"+fileName);   
			//用此方法得到上传文件的真正的文件名，这里的fileName指文件输入类型的名称   
			filePath = mr.getFilesystemName(fileName);   
			System.out.println("FilePath============"+filePath);   
			//此方法得到一个文件对象，代表储存在服务器上的fileName文件   
			File f = mr.getFile(fileName);   
			if (null == f)  {
				System.out.println("======");
			} 
		}   
		//可以取得请求参数的名称   
		Enumeration enum1=mr.getParameterNames();   
		while (enum1.hasMoreElements()) {   
			String s=(String)enum1.nextElement();   
			System.out.println(s);   
			String[] str=mr.getParameterValues(s);   
			for (int i=0;i<str.length;i++){   
				System.out.println(str[i]);   
			}
		} 
		map.put("name",fileName);
	}

	private static void t2(HttpServletRequest request,String saveDirectory,Map<String,String> map) throws IOException{
		MultipartRequest multi=null;     
		//用来限制用户上传文件大小的      
		int maxPostSize = 1 * 100 * 1024 * 1024;     
		//		String saveDirectory = "E:\\test";//服务器上存储路径  
		String dirFlag = System.getProperty("file.separator"); //自动匹配操作系统文件路径  
		//第一个参数为传过来的请求HttpServletRequest，      
		//第二个参数为上传文件要存储在服务器端的目录名称      
		//第三个参数是用来限制用户上传文件大小      
		//第四个参数可以设定用何种编码方式来上传文件名称，可以解决中文问题      
		multi = new MultipartRequest(request, saveDirectory, maxPostSize, "UTF-8");     
		//传回所有文件输入类型的名称      
		Enumeration efs = multi.getFileNames();  
		while(efs.hasMoreElements()){   
			File file = multi.getFile(efs.nextElement().toString());  
			if(file.exists()){  
				String filePath = file.getAbsolutePath();  
				String fname = file.getName();//附件原名  
				System.out.println("上传文件原名============= " +  fname);  
				System.out.println("文件上传地址 =================== " +  filePath);  
				//加密处理  
				//				byte[] bytes1 =  UrlBase64.encode((saveDirectory+dirFlag).getBytes());  
				//获取系统当前时间  
				java.text.SimpleDateFormat date = new java.text.SimpleDateFormat("yyyyMMddHHmmssSS");  
				String currentTimeMillis = date.format(new Date(System.currentTimeMillis()));  

				String t1_fname = fname.substring(0,fname.lastIndexOf("."));  
				String t2_fname = fname.substring(fname.lastIndexOf("."));  
				//重新命名文件  
				fname = t1_fname + "_"+currentTimeMillis + t2_fname;  
				map.put("name", fname);
				//				byte[] bytes2 = UrlBase64.encode(fname.getBytes());  
				//				String encodeNewFilePath = new String(bytes1);  
				//				String encodeNewFileName = new String(bytes2);  
				//				System.out.println("encodeNewFilePath -- >" + encodeNewFilePath+" encodeNewFileName---> "+ encodeNewFileName);  

				File newfile = new File(saveDirectory+dirFlag+fname);  
				file.renameTo(newfile);  
				System.out.println("上传成功！ ");  
			}  
		}     
	}
}
