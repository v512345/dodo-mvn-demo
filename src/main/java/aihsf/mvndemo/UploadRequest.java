package aihsf.mvndemo;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;



public class UploadRequest {
	private final static String   UTF_8  = "UTF-8";
	private static int maxSize = 10 * 1024 * 1024;
	
	/**
	 * 所有支持的MIME TYPES,其他格式的图片不做支持
	 */
	public final static String[] IMG_EXTS   = new String[] { "gif", "jpg", "jpeg","png", "bmp" };
	
	/**
	 * 初始化请求上下文，默认转码为UTF8
	 * 
	 * @param ctx
	 * @param req
	 * @param res
	 */
	public static HttpServletRequest begin(HttpServletRequest request) {
		return autoUploadRequest(encodeRequest(request));
	}
	
	/**
	 * 自动文件上传请求的封装
	 * 
	 * @param req
	 * @return
	 */
	private static HttpServletRequest autoUploadRequest(HttpServletRequest request) {
		if (isMultipart(request)) {
			String path = "F:\\workspace\\mvndemo\\src\\main\\webapp\\upload";
			File dir = new File(path);
			if (!dir.exists() && !dir.isDirectory())
				dir.mkdirs();
			try {
				return new MultipartRequest(request, dir.getCanonicalPath(), maxSize, UTF_8);
			} catch (NullPointerException e) {
				
			} catch (IOException e) {
				
			}
		}
		return request;
	}
	
	/**
	 * 自动编码处理
	 * 
	 * @param req
	 * @return
	 */
	private static HttpServletRequest encodeRequest(HttpServletRequest req) {
		if (req instanceof RequestProxy)
			return req;
		HttpServletRequest autoEncodingReq = req;
		if ("POST".equalsIgnoreCase(req.getMethod())) {
			try {
				autoEncodingReq.setCharacterEncoding(UTF_8);
			} catch (UnsupportedEncodingException e) {
			}
		}
		return autoEncodingReq;
	}
	
	public static boolean isMultipart(HttpServletRequest request) {
		return ((request.getContentType() != null)
				&& (request.getContentType().toLowerCase().startsWith("multipart")));
	}
	
	/**
	 * 自动解码
	 * 
	 */
	private static class RequestProxy extends HttpServletRequestWrapper {
		private String uriEncoding;

		RequestProxy(HttpServletRequest request, String encoding) {
			super(request);
			this.uriEncoding = encoding;
		}

		/**
		 * 重载getParameter
		 */
		public String getParameter(String paramName) {
			String value = super.getParameter(paramName);
			return decodeParamValue(value);
		}

		/**
		 * 重载getParameterMap
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Map<String, String[]> getParameterMap() {
			Map params = super.getParameterMap();
			HashMap<String, String[]> newParams = new HashMap<String, String[]>();
			Iterator<String> iter = params.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String) iter.next();
				Object oValue = params.get(key);
				if (oValue.getClass().isArray()) {
					String[] values = (String[]) params.get(key);
					String[] new_values = new String[values.length];
					for (int i = 0; i < values.length; i++)
						new_values[i] = decodeParamValue(values[i]);

					newParams.put(key, new_values);
				} else {
					String value = (String) params.get(key);
					String newValue = decodeParamValue(value);
					String[] new_values = new String[1];
					new_values[0]=newValue;
					if (newValue != null)
						newParams.put(key, new_values);
				}
			}
			return newParams;
		}

		/**
		 * 重载getParameterValues
		 */
		public String[] getParameterValues(String arg) {
			String[] values = super.getParameterValues(arg);
			for (int i = 0; values != null && i < values.length; i++)
				values[i] = decodeParamValue(values[i]);
			return values;
		}

		/**
		 * 参数转码
		 * 
		 * @param value
		 * @return
		 */
		private String decodeParamValue(String value) {
			if (StringUtils.isBlank(value) || StringUtils.isBlank(uriEncoding)
					|| StringUtils.isNumeric(value))
				return value;
			try {
				return new String(value.getBytes("8859_1"), uriEncoding);
			} catch (Exception e) {
			}
			return value;
		}
	}
	
	
}
